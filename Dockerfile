# syntax=docker/dockerfile:1
FROM denoland/deno:alpine-1.32.5

WORKDIR /app

COPY mod.ts .

RUN echo "@testing https://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories \
    && apk add --no-cache poppler-utils typst@testing

RUN [ "deno", "cache", "mod.ts" ]

CMD [ "deno", "run", "--allow-net", "--allow-env=PORT", "--allow-run=typst,pdftoppm", "--allow-read", "--allow-write", "mod.ts" ]